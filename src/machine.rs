use std::collections::BTreeMap;

enum MemoryAccess {
    None,
    Load(u32, u32),
    Store(u32, u32)
}

pub struct Machine {
    pc: u32,
    regfile: Vec<u32>,
    csrs: Vec<u32>,
    memory: BTreeMap<u32, u32>,
    memory_access_last_cycle: MemoryAccess
}

fn add_signed(a: i32, b: i32) -> u32 {
    use std::num::Wrapping;

    let wr_a = Wrapping(a);
    let wr_b = Wrapping(b);

    let result = wr_a + wr_b;

    result.0 as u32
}

const MISA: usize = 0x301;
const MTVEC: usize = 0x305;
pub const MEPC: usize = 0x341;
pub const MCAUSE: usize = 0x342;
const MHARTID: usize = 0xf14;

#[derive(Clone, Copy, Debug)]
#[repr(u32)]
enum ExceptionCause {
    IllegalInstruction = 2,
    LoadAddressMisaligned = 4,
    StoreAddressMisaligned = 6,
    EcallMMode = 11
}

impl Machine {
    fn init_csrs() -> Vec<u32> {
        let mut csrs = vec![0; 4096];

        csrs[MISA] = 0b0100_0000_0000_0000_0000_0000_0000_0000; // XLEN=32, no extensions
        csrs[MHARTID] = 0;

        csrs
    }

    pub fn new() -> Machine {
        Machine {
            pc: 0,
            regfile: vec![0; 32],
            csrs: Machine::init_csrs(),
            memory: BTreeMap::new(),
            memory_access_last_cycle: MemoryAccess::None
        }
    }

    fn ptr_to_word_index(addr: u32) -> u32 { addr >> 2 }
    
    pub fn dump(&self) {
        use MemoryAccess::*;

        println!("pc: 0x{:08x}", self.pc);

        for i in 0..8 {
            println!("x{:2}-{:2}: 0x{:08x} {:08x} {:08x} {:08x}", i * 4, i * 4 + 3, 
                self.regfile[i * 4], self.regfile[i * 4 + 1],
                self.regfile[i * 4 + 2], self.regfile[i * 4 + 3]);
        }

        match self.memory_access_last_cycle {
            None => (),
            Load(addr, value) => println!("memory: load 0x{:08x} @0x{:08x}",
                value, addr),
            Store(addr, value) => println!("memory: store 0x{:08x} @0x{:08x}",
                value, addr)
        }

        println!();
    }

    pub fn peek_reg(&self, reg: usize) -> u32 {
        self.regfile[reg]
    }

    pub fn peek_word(&mut self, addr: u32) -> u32 {
        assert!(addr % 4 == 0);
        self.load_word(addr, true).unwrap()
    }

    pub fn peek_pc(&self) -> u32 {
        self.pc
    }

    pub fn peek_csr(&self, csr: usize) -> u32 {
        self.csrs[csr]
    }

    pub fn poke_word(&mut self, addr: u32, data: u32) {
        assert!(addr % 4 == 0);
        self.store_word(addr, data);
    }

    pub fn poke_pc(&mut self, pc: u32) {
        self.pc = pc;
    }

    fn load_byte(&mut self, addr: u32, signed: bool, quiet: bool) ->
            Result<u32, ExceptionCause> {
        let word_index = Machine::ptr_to_word_index(addr);
        let value = *self.memory.get(&word_index).unwrap_or(&0);

        // pick a byte and sign/zero-extend
        let masked_byte = match addr % 4 {
            0 => value & 0xff,
            1 => value >> 8 & 0xff,
            2 => value >> 16 & 0xff,
            3 => value >> 24 & 0xff,
            _ => panic!() // should never hit this
        };

        let value_loaded = if signed {
            masked_byte as i8 as i32 as u32
        } else {
            masked_byte as u8 as u32
        };

        if !quiet { self.memory_access_last_cycle = MemoryAccess::Load(addr, value_loaded); }

        Ok(value_loaded)
    }

    fn store_byte(&mut self, addr: u32, value: u8) -> Result<(), ExceptionCause> {
        let word_index = Machine::ptr_to_word_index(addr);
        let existing_value = *self.memory.get(&word_index).unwrap_or(&0);

        // mask a byte out and store it
        let value_to_store = match addr % 4 {
            0 => (existing_value & 0xffff_ff00) | value as u32,
            1 => (existing_value & 0xffff_00ff) | (value as u32) << 8,
            2 => (existing_value & 0xff00_ffff) | (value as u32) << 16,
            3 => (existing_value & 0x00ff_ffff) | (value as u32) << 24,
            _ => panic!() // should never hit this
        };

        self.memory.insert(word_index, value_to_store);
        self.memory_access_last_cycle = MemoryAccess::Store(addr, value as u32);

        Ok(())
    }

    fn load_half(&mut self, addr: u32, signed: bool, quiet: bool) -> 
            Result<u32, ExceptionCause> {
        let word_index = Machine::ptr_to_word_index(addr);
        let value = *self.memory.get(&word_index).unwrap_or(&0);

        // pick 2 bytes and sign/zero-extend
        let masked_half = match addr % 4 {
            0 => value & 0xffff,
            2 => value >> 16 & 0xffff,
            _ => return Err(ExceptionCause::LoadAddressMisaligned)
        };

        let value_loaded = if signed {
            masked_half as i16 as i32 as u32
        } else {
            masked_half as u16 as u32
        };

        if !quiet { self.memory_access_last_cycle = MemoryAccess::Load(addr, value_loaded); }

        Ok(value_loaded)
    }

    fn store_half(&mut self, addr: u32, value: u16) -> Result<(), ExceptionCause> {
        let word_index = Machine::ptr_to_word_index(addr);
        let existing_value = *self.memory.get(&word_index).unwrap_or(&0);

        // mask 2 bytes out and store them
        let value_to_store = match addr % 4 {
            0 => (existing_value & 0xffff_0000) | value as u32,
            2 => (existing_value & 0x0000_ffff) | (value as u32) << 16,
            _ => return Err(ExceptionCause::StoreAddressMisaligned)
        };

        self.memory.insert(word_index, value_to_store);
        self.memory_access_last_cycle = MemoryAccess::Store(addr, value as u32);

        Ok(())
    }

    fn load_word(&mut self, addr: u32, quiet: bool) -> Result<u32, ExceptionCause> {
        if addr % 4 != 0 { return Err(ExceptionCause::LoadAddressMisaligned) }

        let word_index = Machine::ptr_to_word_index(addr);
        let value = *self.memory.get(&word_index).unwrap_or(&0);

        if !quiet { self.memory_access_last_cycle = MemoryAccess::Load(addr, value); }

        Ok(value)
    }

    fn store_word(&mut self, addr: u32, value: u32) -> Result<(), ExceptionCause> {
        if addr % 4 != 0 { return Err(ExceptionCause::StoreAddressMisaligned) }

        let word_index = Machine::ptr_to_word_index(addr);

        self.memory.insert(word_index, value);
        self.memory_access_last_cycle = MemoryAccess::Store(addr, value);

        Ok(())
    }

    fn unpack_imm_i(insn: u32) -> i32 { insn as i32 >> 20 }

    fn unpack_imm_s(insn: u32) -> i32 {
        let imm_b11_5 = (insn & 0b1111_1110_0000_0000_0000_0000_0000_0000) as i32 >> 20;
        let imm_b4_0  = (insn & 0b0000_0000_0000_0000_0000_1111_1000_0000) as i32 >> 7;

        imm_b11_5 | imm_b4_0
    }

    fn unpack_imm_b(insn: u32) -> i32 {
        let imm_b12   =  (insn & 0b1000_0000_0000_0000_0000_0000_0000_0000) as i32  >> 19;
        let imm_b10_5 =  (insn & 0b0111_1110_0000_0000_0000_0000_0000_0000) as i32  >> 20;
        let imm_b4_1  =  (insn & 0b0000_0000_0000_0000_0000_1111_0000_0000) as i32  >> 7;
        let imm_b11   = ((insn & 0b0000_0000_0000_0000_0000_0000_1000_0000) as i32) << 4;

        imm_b12 | imm_b11 | imm_b10_5 | imm_b4_1
    }

    fn unpack_imm_u(insn: u32) -> i32 { (insn & 0xfffff000) as i32 }

    fn unpack_imm_j(insn: u32) -> i32 {
        let imm_b20    = (insn & 0b1000_0000_0000_0000_0000_0000_0000_0000) as i32 >> 11;
        let imm_b10_1  = (insn & 0b0111_1111_1110_0000_0000_0000_0000_0000) as i32 >> 20;
        let imm_b11    = (insn & 0b0000_0000_0001_0000_0000_0000_0000_0000) as i32 >> 9;
        let imm_b19_12 = (insn & 0b0000_0000_0000_1111_1111_0000_0000_0000) as i32;

        imm_b20 | imm_b19_12 | imm_b11 | imm_b10_1
    }

    fn write_reg(&mut self, i: usize, value: u32) {
        // implement x0 being wired to ABZ
        if i != 0 { self.regfile[i] = value; }
    }

    fn trap(&mut self, cause: ExceptionCause) {
        let mtvec = self.csrs[MTVEC];
        let vector = mtvec & 0xffff_fffc;
        let mode = mtvec & 0x3;
        
        // println!("trap 0x{:08x} at 0x{:08x}: vector 0x{:08x}, mode {}", cause as u32, 
        //     self.pc, vector, mode);

        self.csrs[MEPC] = self.pc;
        self.csrs[MCAUSE] = cause as u32;
        self.pc = vector;
    }

    pub fn step(&mut self) {
        let exc = self.do_step();

        match exc {
            Some(cause) => {
                self.trap(cause);
            },
            _ => ()
        }
    }

    fn do_step(&mut self) -> Option<ExceptionCause> {
        use ExceptionCause::*;

        self.memory_access_last_cycle = MemoryAccess::None;

        let insn = *self.memory.get(&Machine::ptr_to_word_index(self.pc)).
            unwrap_or(&0);
        let mut next_pc = self.pc + 4;
        let opcode = insn & 0b1111111;
        let rd = ((insn >> 7) & 0b11111) as usize;
        let rs1 = ((insn >> 15) & 0b11111) as usize;
        let rs2 = ((insn >> 20) & 0b11111) as usize;
        let funct3 = (insn >> 12) & 0b111;
        let funct7 = (insn >> 25) & 0b1111111;

        match (opcode, funct3, funct7) {
            (0b000_0011, _, _) /* load */ => {
                let imm = Machine::unpack_imm_i(insn);
                let addr = self.regfile[rs1] as i32 + imm;

                let load_result = match funct3 {
                    0b000 /* LB */ => self.load_byte(addr as u32, true, false),
                    0b001 /* LH */ => self.load_half(addr as u32, true, false),
                    0b010 /* LW */ => self.load_word(addr as u32, false),
                    0b100 /* LBU */ => self.load_byte(addr as u32, false, false),
                    0b101 /* LHU */ => self.load_half(addr as u32, false, false),
                    _ => return Some(IllegalInstruction)
                };

                match load_result {
                    Ok(value) => self.write_reg(rd, value),
                    Err(cause) => return Some(cause)
                };
            },
            (0b000_1111, _, _) /* FENCE; no-op for now */ => (),
            (0b001_0011, _, _) /* I-type */ => {
                let imm = Machine::unpack_imm_i(insn);
                let acc_signed = self.regfile[rs1] as i32;
                let acc_unsigned = self.regfile[rs1];
                let shamt = imm as u32 & 0b11111;

                let result = match (funct3, funct7) {
                    (0b000, _) /* ADDI */ => add_signed(acc_signed, imm),
                    (0b001, 0b0000000) /* SLLI */ => acc_unsigned << shamt,
                    (0b010, _) /* SLTI */ => if acc_signed < imm { 1 } else { 0 },
                    (0b011, _) /* SLTIU */ => if acc_unsigned < imm as u32 { 1 } else { 0 },
                    (0b100, _) /* XORI */ => acc_unsigned ^ imm as u32,
                    (0b101, 0b0000000) /* SRLI */ => acc_unsigned >> shamt,
                    (0b101, 0b0100000) /* SRAI */ => (acc_signed >> shamt) as u32,
                    (0b110, _) /* ORI */ => acc_unsigned | imm as u32,
                    (0b111, _) /* ANDI */ => acc_unsigned & imm as u32,
                    _ => return Some(IllegalInstruction)
                };

                self.write_reg(rd, result);
            },
            (0b001_0111, _, _) /* AUIPC */ => {
                use std::num::Wrapping;
                let pc_w = Wrapping(self.pc);
                let imm_w = Wrapping(Machine::unpack_imm_u(insn) as u32);

                self.write_reg(rd, (pc_w + imm_w).0);
            },
            (0b010_0011, _, _) /* store */ => {
                let imm = Machine::unpack_imm_s(insn);
                let addr = self.regfile[rs1] as i32 + imm;
                let value = self.regfile[rs2];

                let store_result = match funct3 {
                    0b000 /* SB */ => self.store_byte(addr as u32, value as u8),
                    0b001 /* SH */ => self.store_half(addr as u32, value as u16),
                    0b010 /* SW */ => self.store_word(addr as u32, value),
                    _ => return Some(IllegalInstruction)
                };

                match store_result {
                    Err(cause) => return Some(cause),
                    _ => ()
                }
            },
            (0b011_0011, _, _) /* R-type */ => {
                let a_unsigned = self.regfile[rs1];
                let b_unsigned = self.regfile[rs2];
                let a_signed = self.regfile[rs1] as i32;
                let b_signed = self.regfile[rs2] as i32;
                let shamt = b_unsigned & 0b11111;

                let result = match (funct3, funct7) {
                    (0b000, 0b0000000) /* ADD */ => add_signed(a_signed, b_signed),
                    (0b000, 0b0100000) /* SUB */ => add_signed(a_signed, -b_signed),
                    (0b001, 0b0000000) /* SLL */ => a_unsigned << shamt,
                    (0b010, 0b0000000) /* SLT */ => if a_signed < b_signed { 1 } else { 0 },
                    (0b011, 0b0000000) /* SLTU */ => if a_unsigned < b_unsigned { 1 } else { 0 },
                    (0b100, 0b0000000) /* XOR */ =>  a_unsigned ^ b_unsigned,
                    (0b101, 0b0000000) /* SRL */ => a_unsigned >> shamt,
                    (0b101, 0b0100000) /* SRA */ => (a_signed >> shamt) as u32,
                    (0b110, 0b0000000) /* OR */ => a_unsigned | b_unsigned,
                    (0b111, 0b0000000) /* AND */ => a_unsigned & b_unsigned,
                    _ => return Some(IllegalInstruction)
                };

                self.write_reg(rd, result);
            },
            (0b011_0111, _, _) /* LUI */ => {
                let imm = Machine::unpack_imm_u(insn);
                self.write_reg(rd, imm as u32);
            },
            (0b110_0011, _, _) /* B-type */ => {
                let a = self.regfile[rs1] as i32;
                let b = self.regfile[rs2] as i32;
                let imm = Machine::unpack_imm_b(insn);

                let branch_taken = match funct3 {
                    0b000 /* BEQ */ => a == b,
                    0b001 /* BNE */ => a != b,
                    0b100 /* BLT */ => a < b,
                    0b101 /* BGE */ => a >= b,
                    _ => return Some(IllegalInstruction)
                };

                if branch_taken { next_pc = (self.pc as i32 + imm) as u32 }
            },
            (0b110_0111, _, _) /* JALR */ => {
                let imm = Machine::unpack_imm_i(insn);
                self.write_reg(rd, next_pc);
                next_pc = (self.regfile[rs1] as i32 + imm) as u32;
            },
            (0b110_1111, _, _) /* JAL */ => {
                let imm = Machine::unpack_imm_j(insn);
                self.write_reg(rd, next_pc);
                next_pc = (self.pc as i32 + imm) as u32;
            },
            (0b111_0011, 0b000, 0b000_0000) /* ECALL/EBREAK */ => {
                self.trap(EcallMMode);
                next_pc = self.pc; // trap overwrites the PC
            },
            (0b111_0011, 0b000, 0b000_1000) /* WFI; no-op for now */ => (),
            (0b111_0011, 0b000, 0b001_1000) /* MRET */ => {
                next_pc = self.csrs[MEPC];
            },
            (0b111_0011, _, _) /* Zicsr */ => {
                let csr = Machine::unpack_imm_i(insn) & 0xfff;
                let old_csr_value = self.csrs[csr as usize];
                let new_csr_value: u32 = match funct3 {
                    0b001 /* CSRRW */ => self.regfile[rs1],
                    0b010 /* CSRRS */ => old_csr_value | self.regfile[rs1],
                    0b011 /* CSRRC */ => old_csr_value & !self.regfile[rs1],
                    0b101 /* CSRRWI */ => rs1 as u32,
                    0b110 /* CSRRSI */ => old_csr_value | rs1 as u32,
                    0b111 /* CSRRCI */ => old_csr_value & !(rs1 as u32),
                    _ => return Some(IllegalInstruction)
                };

                self.csrs[csr as usize] = new_csr_value;
                self.write_reg(rd, old_csr_value);
            }
            _ /* illegal instruction */ => return Some(IllegalInstruction)
        };

        self.pc = next_pc;

        None
    }
}