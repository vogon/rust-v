/*
	test program: compute the first 10 terms of the fibonacci sequence into
	0x1000-0x1028; verified and assembled by the Venus simulator
	(http://venus.cs61c.org/)

	setup:
	  li sp, 0x4000             # set up stack pointer
	  li gp, 0x1000             # set up heap
	  li a0, 10
	  jal fib                   # do the math(s)
	  ret						# overwrite with illegal instruction to halt
	fib:
	  addi t0, zero, 2
	  bgt a0, t0, fibrec_1      # degenerate case?
	  addi a1, zero, 1          # if so, return 1
	  j memoize
	fibrec_1:
	  addi t0, a0, -1           # look for fib(a0 - 1) memo
	  slli t0, t0, 2            # compute offset
	  add t0, t0, gp            # compute pointer
	  lw t0, 0(t0)              # load memo
	  beqz t0, recurse_1        # is memo present?
	  mv t1, t0					# move it to the temporary
	  j fibrec_2
	recurse_1:
	  addi sp, sp, -8           # save regs
	  sw ra, 4(sp)
	  sw a0, 0(sp)
	  addi a0, a0, -1           # compute fib(a0 - 1)
	  jal fib
	  mv t1, a0                 # save result in temporary
	  lw ra, 4(sp)              # reload regs
	  lw a0, 0(sp)
	  addi sp, sp, 8
	fibrec_2:
	  addi t0, a0, -2           # look for fib(a0 - 2) memo
	  slli t0, t0, 2            # compute offset
	  add t0, t0, gp            # compute pointer
	  lw t0, 0(t0)              # load memo
	  beqz t0, recurse_2        # is memo present?
	  mv t2, t0                 # move it to the temporary
	  j done_2
	recurse_2:
	  addi sp, sp, -12          # save regs
	  sw ra, 8(sp)
	  sw t1, 4(sp)
	  sw a0, 0(sp)
	  addi a0, a0, -2           # compute fib(a0 - 2)
	  jal fib
	  mv t2, a0                 # save result in temporary
	  lw ra, 8(sp)              # reload regs
	  lw t1, 4(sp)
	  lw a0, 0(sp)
	  addi sp, sp, 12
	done_2:
	  add a1, t1, t2            # add em up
	memoize:
	  slli t0, a0, 2            # compute offset into heap
	  add t0, t0, gp            # compute pointer
	  sw a1, 0(t0)              # store memo
	  mv a0, a1                 # move result into first rv
	  ret
 */
pub fn fib() -> Vec<u32> {
	vec![
		0x00004137,	// lui x2 4			li sp, 0x4000 # set up stack pointer
		0x00010113,	// addi x2 x2 0		li sp, 0x4000 # set up stack pointer
		0x000011B7,	// lui x3 1			li gp, 0x1000 # set up heap
		0x00018193,	// addi x3 x3 0		li gp, 0x1000 # set up heap
		0x00A00513,	// addi x10 x0 10	li a0, 10
		0x008000EF,	// jal x1 8			jal fib # do the math(s)
		0xFFFFFFFF,	// illegal instruction to halt VM
		0x00200293,	// addi x5 x0 2		addi t0, zero, 2
		0x00A2C663,	// blt x5 x10 12	bgt a0, t0, fibrec_1 # degenerate case?
		0x00100593,	// addi x11 x0 1	addi a1, zero, 1 # if so, return 1
		0x0900006F,	// jal x0 144		j memoize
		0xFFF50293,	// addi x5 x10 -1	addi t0, a0, -1 # look for fib(a0 - 1) memo
		0x00229293,	// slli x5 x5 2		slli t0, t0, 2 # compute offset
		0x003282B3,	// add x5 x5 x3		add t0, t0, gp # compute pointer
		0x0002A283,	// lw x5 0(x5)		lw t0, 0(t0) # load memo
		0x00028663,	// beq x5 x0 12		beqz t0, recurse_1 # is memo present?
		0x00028313,	// addi x6 x5 0		mv t1, t0 # move it to the temporary
		0x0280006F,	// jal x0 40		j fibrec_2
		0xFF810113,	// addi x2 x2 -8	addi sp, sp, -8 # save regs
		0x00112223,	// sw x1 4(x2)		sw ra, 4(sp)
		0x00A12023,	// sw x10 0(x2)		sw a0, 0(sp)
		0xFFF50513,	// addi x10 x10 -1	addi a0, a0, -1 # compute fib(a0 - 1)
		0xFC5FF0EF,	// jal x1 -60		jal fib
		0x00050313,	// addi x6 x10 0	mv t1, a0 # save result in temporary
		0x00412083,	// lw x1 4(x2)		lw ra, 4(sp) # reload regs
		0x00012503,	// lw x10 0(x2)		lw a0, 0(sp)
		0x00810113,	// addi x2 x2 8		addi sp, sp, 8
		0xFFE50293,	// addi x5 x10 -2	addi t0, a0, -2 # look for fib(a0 - 2) memo
		0x00229293,	// slli x5 x5 2		slli t0, t0, 2 # compute offset
		0x003282B3,	// add x5 x5 x3		add t0, t0, gp # compute pointer
		0x0002A283,	// lw x5 0(x5)		lw t0, 0(t0) # load memo
		0x00028663,	// beq x5 x0 12		beqz t0, recurse_2 # is memo present?
		0x00028393,	// addi x7 x5 0		mv t2, t0 # move it to the temporary
		0x0300006F,	// jal x0 48		j done_2
		0xFF410113,	// addi x2 x2 -12	addi sp, sp, -12 # save regs
		0x00112423,	// sw x1 8(x2)		sw ra, 8(sp)
		0x00612223,	// sw x6 4(x2)		sw t1, 4(sp)
		0x00A12023,	// sw x10 0(x2)		sw a0, 0(sp)
		0xFFE50513,	// addi x10 x10 -2	addi a0, a0, -2 # compute fib(a0 - 2)
		0xF81FF0EF,	// jal x1 -128		jal fib
		0x00050393,	// addi x7 x10 0	mv t2, a0 # save result in temporary
		0x00812083,	// lw x1 8(x2)		lw ra, 8(sp) # reload regs
		0x00412303,	// lw x6 4(x2)		lw t1, 4(sp)
		0x00012503,	// lw x10 0(x2)		lw a0, 0(sp)
		0x00C10113,	// addi x2 x2 12	addi sp, sp, 12
		0x007305B3,	// add x11 x6 x7	add a1, t1, t2 # add em up
		0x00251293,	// slli x5 x10 2	slli t0, a0, 2 # compute offset into heap
		0x003282B3,	// add x5 x5 x3		add t0, t0, gp # compute pointer
		0x00B2A023,	// sw x11 0(x5)		sw a1, 0(t0) # store memo
		0x00058513,	// addi x10 x11 0	mv a0, a1 # move result into first rv
		0x00008067,	// jalr x0 x1 0		ret
	]
}
