mod elf_image;
mod fib_asm;
mod machine;

// use fib_asm::fib;
use std::fs;
use std::io;
use std::io::Write;
use elf_image::ElfImage;
use machine::Machine;
use regex::Regex;

fn run_file(path: &str) -> bool {
    let mut riscv = Machine::new();

    let image = fs::read(path).expect("couldn't load test");
    let elf = ElfImage::parse(image.as_slice()).expect("can't parse binary");

    elf.load_into(&mut riscv);
    riscv.poke_pc(elf.start_address());

    let mut cycles = 0;

    loop {
        riscv.step();
        cycles += 1;

        // check for test success/failure
        let tohost = riscv.peek_word(0x80001000);
        if tohost != 0 {
            // test signals completion status by calling linux syscall 93 (exit);
            // reg 10 is a0, which should contain the first argument
            match riscv.peek_reg(10) {
                0 => {
                    println!("passed.");
                    return true;
                },
                exit_code => {
                    println!("failed with exit code {}!", exit_code);
                    return false;
                }
            }
        }

        // check to see if we're in the exception handler
        let gp = riscv.peek_reg(3);
        if gp == 1337 {
            println!("threw exception (cause = 0x{:08x}, epc = 0x{:08x})!  state dump follows.",
                riscv.peek_csr(machine::MCAUSE), riscv.peek_csr(machine::MEPC));
            riscv.dump();
            return false;
        }
        
        // check to see if we've been running for a while without terminating
        if cycles == 1000000 {
            println!("didn't terminate after 1m cycles!  state dump follows.");
            riscv.dump();
            return false;
        }
    }
}

fn main() -> std::io::Result<()> {
    let mut failed = 0;
    let mut passed = 0;

    for entry in fs::read_dir("/opt/riscv/target/share/riscv-tests/isa")? {
        let dir = entry?;

        let name = dir.file_name().into_string().unwrap();
        let mut execute = true;

        // skip human-readable dump files
        if name.ends_with(".dump") { execute = false; }

        // skip files for other variants of RISC-V
        let re = Regex::new(r"^rv32.i-p").unwrap();
        if !re.is_match(name.as_str()) { execute = false; }

        if !execute { continue; }

        print!("{}... ", dir.file_name().into_string().unwrap());
        io::stdout().flush()?;
        let result = run_file(dir.path().to_str().unwrap());

        if result { passed += 1; } else { failed += 1; }
    }
    
    println!("{} tests passed, {} failed.", passed, failed);

    Ok(())
}
