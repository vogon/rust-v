use std::convert::TryInto;
use std::io::Cursor;
use binread::{BinRead, BinReaderExt};
use derivative::Derivative;

use crate::machine::Machine;

#[derive(BinRead, Debug)]
#[br(magic = b"\x7fELF")]
struct ElfIdent {
    ei_class: u8,
    ei_data: u8,
    ei_version: u8
}

#[derive(BinRead, Debug)]
struct ElfHeader {
    #[br(align_after = 16)]
    e_ident: ElfIdent,

    e_type: u16,
    e_machine: u16,
    e_version: u32,
    e_entry: u32,
    e_phoff: u32,
    e_shoff: u32,
    e_flags: u32,
    e_ehsize: u16,
    e_phentsize: u16,
    e_phnum: u16,
    e_shentsize: u16,
    e_shnum: u16,
    e_shstrndx: u16
}

#[derive(BinRead, Debug)]
enum ProgramHeaderType {
    #[br(magic = 0u32)] Null,
    #[br(magic = 1u32)] Load,
    #[br(magic = 2u32)] Dynamic,
    #[br(magic = 3u32)] Interp,
    #[br(magic = 4u32)] Note,
    #[br(magic = 5u32)] ShLib,
    #[br(magic = 6u32)] PHdr
}

#[derive(BinRead, Debug)]
struct ProgramHeader {
    p_type: ProgramHeaderType,
    p_offset: u32,
    p_vaddr: u32,
    p_paddr: u32,
    p_filesz: u32,
    p_memsz: u32,
    p_flags: u32,
    p_align: u32
}

#[derive(BinRead, Debug)]
struct SectionHeader {
    sh_name: u32,
    sh_type: u32,
    sh_flags: u32,
    sh_addr: u32,
    sh_offset: u32,
    sh_size: u32,
    sh_link: u32,
    sh_info: u32,
    sh_addralign: u32,
    sh_entsize: u32
}

#[derive(Derivative)]
#[derivative(Debug)]
pub struct ElfImage<'a> {
    #[derivative(Debug="ignore")] data: &'a [u8],
    ehdr: ElfHeader,
    program_headers: Vec<ProgramHeader>,
    section_headers: Vec<SectionHeader>
}

impl<'a> ElfImage<'a> {
    fn read_image_endian<T: BinRead>(buf: &mut Cursor<&[u8]>, ei_data: u8) ->
            Result<T, String> {
        match ei_data {
            0x1 => Ok(buf.read_le().unwrap()),
            0x2 => Ok(buf.read_be().unwrap()),
            _ => Err(format!("unexpected endianness 0x{:02x}", ei_data))
        }
    }

    pub fn parse(data: &[u8]) -> Result<ElfImage, String> {
        let mut buf = Cursor::new(data);

        // check the ELF ident
        let ident: ElfIdent = buf.read_ne().expect("couldn't parse ident");

        match ident.ei_class {
            1 => (),
            2 => return Err(String::from("64-bit ELF not supported yet")),
            _ => return Err(format!("unrecognized class 0x{:02x}", ident.ei_class))
        }

        if ident.ei_version != 1 {
            return Err(format!("unrecognized version 0x{:02x}", ident.ei_version))
        }

        // rewind and read the ELF header
        buf.set_position(0);
        let ehdr: ElfHeader = ElfImage::read_image_endian(&mut buf, 
            ident.ei_data).unwrap();

        // check the ELF header
        if ehdr.e_type != 2 {
            return Err(String::from("only ELF executables supported"))
        }

        if ehdr.e_machine != 243 {
            return Err(String::from("not a RISC-V binary"))
        }

        // read program headers
        let mut program_headers = Vec::new();
        buf.set_position(ehdr.e_phoff.into());

        for _i in 0..ehdr.e_phnum {
            let header: ProgramHeader = ElfImage::read_image_endian(&mut buf,
                ident.ei_data).unwrap();
            
            program_headers.push(header);
        }

        // read section headers
        let mut section_headers = Vec::new();
        buf.set_position(ehdr.e_shoff.into());

        for _i in 0..ehdr.e_shnum {
            let header: SectionHeader = ElfImage::read_image_endian(&mut buf,
                ident.ei_data).unwrap();

            section_headers.push(header);
        }

        let img = ElfImage {
            data, ehdr, program_headers, section_headers
        };

        // println!("{:?}", img);

        Ok(img)
    }

    pub fn load_into(&self, machine: &mut Machine) {
        for header in &self.program_headers {
            for offset in (0..header.p_filesz).step_by(4) {
                let file_offset = (header.p_offset + offset) as usize;
                
                machine.poke_word(header.p_paddr + offset,
                    u32::from_le_bytes(self.data[file_offset..(file_offset + 4)].
                        try_into().unwrap()));
            }
        }
    }

    pub fn start_address(&self) -> u32 { self.ehdr.e_entry }
}